<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;



/**
 * Class PostController
 * @package AppBundle\Controller
 * @Route("/")
 */
class PostController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        if($this->getUser()){
            return $this->redirectToRoute('type');
        }

        return $this->render('@App/Post/index.html.twig', array());
    }

    /**
     * @Route("/profil", name="profil")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function myProfil(Request $request){
        $user = $this->getUser();
        if($user){
            $image =  $this->getUser()->getPosts();
            $followers = $user->getFollows();
            $post = new Post();
            $form = $this->createForm(PostType::class, $post);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $post->setAuthor($user);
                $em->persist($post);
                $em->flush();
                return $this->redirectToRoute('profil');
            }
            return $this->render('@App/Post/myProfil.html.twig', array(
                'form' => $form->createView(),
                'images'=> $image,
                'followers'=>$followers
            ));
        }
        return $this->render('@App/Post/index.html.twig', array());
}

    /**
     * @Route("/type", name="type")
     */
    public function typeAction(){

        $user = $this->getUser();
        if($user){

            $image = $this->getDoctrine()
                ->getRepository('AppBundle:Post')
                ->findAll();

            return $this->render('@App/Post/type.html.twig', array(
                'images' => $image,
                'user'=> $user
            ));
        }
        return $this->render('@App/Post/index.html.twig', array());
    }

    /**
     *
     * @Route("/like/{id}", requirements={"id" : "\d+"})
    )
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function likersAction(int $id){

        $curentUser = $this->getUser();

            $post = $this->getDoctrine()
                ->getRepository('AppBundle:Post')
                ->find($id);

        if(!($post->hasLikers($curentUser))) {
            $em = $this->getDoctrine()
                ->getManager();
            $post->addLiker($curentUser);
            $em->flush();
        }else {
            $em = $this->getDoctrine()
                ->getManager();
            $post->removeLiker($curentUser);
            $em->flush();
            }


        return $this->redirectToRoute('type');
    }


    /**
     *
     * @Route("/fallow/{id}", requirements={"id" : "\d+"})
    )
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function fallowAction(int $id){

        $curentUser = $this->getUser();

        $repository= $this->getDoctrine()
            ->getRepository('AppBundle:User');

        $user = $repository->find($id);
        $user2 = $repository->find($curentUser->getId());

        $user->addFollow($user2);
        $em = $this->getDoctrine()
            ->getManager();
        $em->flush();

        return $this->redirectToRoute('type');
    }

    /**
     *
     * @Route("/remove_fallow/{id}", requirements={"id" : "\d+"})
    )
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeFallowAction(Request $request , int $id){

        $curentUser = $this->getUser();

        $repository= $this->getDoctrine()
            ->getRepository('AppBundle:User');

        $user = $repository->find($id);
        $user2 = $repository->find($curentUser->getId());

        $user->removeFollow($user2);
        $em = $this->getDoctrine()
            ->getManager();
        $em->flush();

        $referer = $request
            ->headers
            ->get('referer');
        return $this->redirect($referer);
        /*return $this->redirectToRoute('type');*/
    }
}

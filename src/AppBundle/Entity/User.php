<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Post", mappedBy="likers")
     */
    private $likedPosts;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Post", mappedBy="author" )
     */
    private $posts;


    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User",mappedBy="follows")
     */
    private $followers;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="User",inversedBy="followers")
     *
     * @ORM\JoinTable(name="fallowers",joinColumns={@ORM\JoinColumn(name="fallower_id",referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="fallowed_id",referencedColumnName="id")}
     *     )
     */
    private $follows;


    public function __construct()
    {
        parent::__construct();
        $this->posts = new ArrayCollection();
        $this->likedPosts = new ArrayCollection();
        $this->followers = new ArrayCollection();
        $this->follows = new ArrayCollection();
    }

    /**
     * @param Post $post
     */
     public function removePost(Post $post)
     {
        $this->posts->removeElement($post);
     }

    /**
     * @param Post $post
     */
    public function addPost(Post $post)
    {
        $this->posts->add($post);
    }

    /**
     * @return ArrayCollection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param Post $post
     */
    public function removeLikedPosts(Post $post)
    {
        $this->likedPosts->removeElement($post);
    }

    /**
     * @param Post $post
     */
    public function addLikedPosts(Post $post)
    {
        $this->likedPosts->add($post);
    }

    /**
     * @return ArrayCollection
     */
    public function getLikedPosts(): ArrayCollection
    {
        return $this->likedPosts;
    }

    /**
     * @return mixed
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * @return mixed
     */
    public function getFollows()
    {
        return $this->follows;
    }
    /**
     * @param User $user
     */
    public function addFollower(User $user)
    {
        $this->followers->add($user);
    }

    /**
     * @param User $user
     */
    public function addFollow(User $user)
    {
        $this->follows->add($user);
    }

    /**
     * @param User $user
     */
    public function removeFollower(User $user)
    {
        $this->followers->removeElement($user);
    }

    /**
     * @param User $user
     */
    public function removeFollow(User $user)
    {
        $this->follows->removeElement($user);
    }
    /**
     * @param User $user
     *
     */
 public function hasFollowers(User $user){

     return   $this->followers->contains($user);
 }

}
